#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Coded by neib.


from tkinter import *
import os


#----------------------- Find ink level -----------------------#
def ink(lineNumber):

    #First test with USB    
    inkCommand = os.popen("ink -p usb")
    #line = inkCommand.read()
    line0 = inkCommand.readline()
       
    if(line0=="Could not get device id.\n" or line0=="No printer found.\n"):
        #Reset
        inkCommand.close()
        line0=""
        #Then test with network
        inkCommand = os.popen("ink -p bjnp")
        line0 = inkCommand.readline()
        
        #No printer found
        if(line0=="Could not get device id.\n" or line0=="No printer found.\n"):
            line="Could not get device id.\nNo printer found."
            inkCommand.close()
            return line
    
    #Printer found
    else:
        #line0 = inkCommand.readline()
        line1 = inkCommand.readline()
        line2 = inkCommand.readline()
        line3 = inkCommand.readline()
        line4 = inkCommand.readline()
        line5 = inkCommand.readline()
        line6 = inkCommand.readline()
        line7 = inkCommand.readline()
        inkCommand.close()

        if(lineNumber==0):
            line=line0
        elif(lineNumber==1):
            line=line1
        elif(lineNumber==2):
            line=line2
        elif(lineNumber==3):
            line=line3
        elif(lineNumber==4):
            line=line4
        elif(lineNumber==5):
            line=line5
        elif(lineNumber==6):
            line=line6
        else:
            line=line7

        return line


#----------------------- Main window -----------------------#
Window = Tk()
Window.title("Ink Level Informations")

inkCommandTitle = ink(0)

#----------------------- If no errors -----------------------#
if(inkCommandTitle != "Could not get device id.\nNo printer found."):
    #----------------------- Window construction -----------------------#
    #Title
    commandTitle = Label(Window, text=inkCommandTitle.replace('\n',''), font="bold")
    commandTitle.pack()

    #Model
    printerModel = ink(2)
    printer = Label(Window, text=printerModel.replace('\n',''), font="bold")
    printer.pack()

    windFrame = Frame(Window, borderwidth=2, relief="groove")
    windFrame.pack(padx=5)
    #Names
    colorFrame = Frame(windFrame)
    colorFrame.pack(padx=0,side=LEFT)
    #Color bars
    barFrame = Frame(windFrame)
    barFrame.pack(padx=0,side=LEFT)
    #Percent
    percentFrame = Frame(windFrame)
    percentFrame.pack(padx=0,side=LEFT)


    #----------------------- Cyan -----------------------#
    cyan = ink(7)
    cPercent = int(''.join(filter(str.isdigit, cyan)))
    
    #Name
    cColor = Label(colorFrame, text="Cyan:", font="bold")
    cColor.pack()
    
    #Color
    cPercentBar = Frame(barFrame, bg="white", borderwidth=2, relief="groove")
    cPercentBar.config(width=200, height=20)
    cPercentBar.pack_propagate(False)
    cPercentBar.pack(pady=2)
    
    cPercentBarColor = Frame(cPercentBar, bg="cyan")
    cPercentBarColor.config(width=cPercent*2, height=20)#*2 Because parent width=200
    cPercentBarColor.pack_propagate(False)
    cPercentBarColor.pack(side=RIGHT)
    
    #Percent
    cBar = Label(percentFrame, text=str(cPercent)+'%', font="bold")
    cBar.pack()


    #----------------------- Magenta -----------------------#
    magenta = ink(6)
    mPercent = int(''.join(filter(str.isdigit, magenta)))
    
    #Name
    mColor = Label(colorFrame, text="Magenta:", font="bold")
    mColor.pack()

    #Color
    mPercentBar = Frame(barFrame, bg="white", borderwidth=2, relief="groove")
    mPercentBar.config(width=200, height=20)
    mPercentBar.pack_propagate(False)
    mPercentBar.pack(pady=2)

    mPercentBarColor = Frame(mPercentBar, bg="magenta")
    mPercentBarColor.config(width=mPercent*2, height=20)#*2 Because parent width=200
    mPercentBarColor.pack_propagate(False)
    mPercentBarColor.pack(side=RIGHT)

    #Percent
    mBar = Label(percentFrame, text=str(mPercent)+'%', font="bold")
    mBar.pack()


    #----------------------- Yellow -----------------------#
    yellow = ink(5)
    yPercent = int(''.join(filter(str.isdigit, yellow)))
    
    #Name
    yColor = Label(colorFrame, text="Yellow:", font="bold")
    yColor.pack()

    #Color
    yPercentBar = Frame(barFrame, bg="white", borderwidth=2, relief="groove")
    yPercentBar.config(width=200, height=20)
    yPercentBar.pack_propagate(False)
    yPercentBar.pack(pady=2)

    yPercentBarColor = Frame(yPercentBar, bg="yellow")
    yPercentBarColor.config(width=yPercent*2, height=20)#*2 Because parent width=200
    yPercentBarColor.pack_propagate(False)
    yPercentBarColor.pack(side=RIGHT)

    #Percent
    yBar = Label(percentFrame, text=str(yPercent)+'%', font="bold")
    yBar.pack()


    #----------------------- Black -----------------------#
    photoblack = ink(4)
    bPercent = int(''.join(filter(str.isdigit, photoblack)))
    
    #Name
    bColor = Label(colorFrame, text="Black:", font="bold")
    bColor.pack()

    #Color
    bPercentBar = Frame(barFrame, bg="white", borderwidth=2, relief="groove")
    bPercentBar.config(width=200, height=20)
    bPercentBar.pack_propagate(False)
    bPercentBar.pack(pady=2)

    bPercentBarColor = Frame(bPercentBar, bg="black")
    bPercentBarColor.config(width=bPercent*2, height=20)#*2 Because parent width=200
    bPercentBarColor.pack_propagate(False)
    bPercentBarColor.pack(side=RIGHT)

    #Percent
    bBar = Label(percentFrame, text=str(bPercent)+'%', font="bold")
    bBar.pack()


    #----------------------- Exit button -----------------------#
    exitButton = Button(Window, text="Quit", command=Window.destroy)
    exitButton.pack(pady=5, side=BOTTOM)

    #----------------------- Launch window -----------------------#
    Window.mainloop()


#----------------------- If errors keeps going -----------------------#
else:
    #----------------------- Window construction -----------------------#
    #Title
    errorFrame = Frame(Window, borderwidth=2, relief="groove", bg="white")
    errorFrame.pack(padx=25, pady=5)
    commandTitle = Label(errorFrame, text=inkCommandTitle, font="bold", bg="white")
    commandTitle.pack(padx=5,pady=5)
    #----------------------- Exit button -----------------------#
    exitButton = Button(Window, text="Quit", command=Window.destroy)
    exitButton.pack(pady=5, side=BOTTOM)
    #----------------------- Launch window -----------------------#
    Window.mainloop()
