# README #

### What is this repository for? ###

* A Tk frontend for Ink command line tool.
* Version-1.0

### How do I get set up? ###

* Just plug your printer and run.
* No specific configuration.
* Depends on the "ink" package.

### Who do I talk to? ###

* Coded by neib under Osef Licence.